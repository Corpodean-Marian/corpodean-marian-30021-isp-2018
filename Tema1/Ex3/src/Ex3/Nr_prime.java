package Ex3;

     
import static java.lang.Math.sqrt;
import java.util.Scanner;
        
public class Nr_prime {
    
static boolean prime(int x){
    if(x==2) return true;
    if(x%2==0||x<2){
        return false;
    }
    for(int i=3;i<=sqrt(x);i++){
        if(x%i==0) return false;
    }
    return true;
}
    
public static void main(String[] args) {
       Scanner n = new Scanner (System.in);
            System.out.println("Introd pe A:");
        int A = n.nextInt();
            System.out.println("Introd pe B:");
        int B = n.nextInt();
        if(A>B)
        {
            int C;
            C = A;
            A = B;
            B = C;
        }
        for(int i=A;i<B;i++){
            if(prime(i)) System.out.println(i+" ");
        }
}
}