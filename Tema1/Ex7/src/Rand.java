import java.util.*;

public class Rand {
    public static void main(String[] args) {
        Random r = new Random();
        Scanner in = new Scanner(System.in);
        int number = r.nextInt(100);
        int counter = 0;
        while (counter < 3){
            int t = in.nextInt();
            counter++;
            if(t==number) {
                System.out.println("Win");
                break;
            }
            if(t<number) System.out.println("Wrong answer, your number is too low");
            if(t>number) System.out.println("Wrong answer, your number is too high");        
        }
    if(counter==3) System.out.println("You lose \n The number was "+number); 
}


}
