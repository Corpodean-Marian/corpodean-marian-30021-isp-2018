package robot.machine;
class Engine {
            private int x=0,y=0;
            boolean carry=false;
            void step(int x,int y) {
                        //start();
                        //move(x,y);
                        execute(x,y);
                        //stop();
            }
 
            private void start(){
                        System.out.println("Start engine.");
            }
            private void stop(){
                        System.out.println("Stop engine.");
            }
 
            private void move(int x,int y){
                        if(x<0) System.out.println("Moving to the left.");
                        else System.out.println("Moving to the right.");
                        if(y<0) System.out.println("Moving forwards.");
                        else System.out.println("Moving backwards.");
                        
            }
            
            public boolean pick(boolean k){
                        if(carry!=k){
                        carry=k;
                        if(k) System.out.println("Picked object from " + x + " " + y);
                        else System.out.println("Dropped object to " + x + " " + y);
                        return true;
                        }
                        else System.out.println("Already carrying object");
                            return false;
            }
 
            private void execute(int a,int b){
                        x+=a;
                        y+=b;
                        System.out.println("New position " + x + " " + y);
                        
            }
 }