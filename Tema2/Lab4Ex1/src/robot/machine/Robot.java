package robot.machine;
public class Robot {
            Engine robotEngine;
            public Robot(){
                        robotEngine = new Engine();
            }
 
            public void moveRobot(int x,int y){
                        robotEngine.step(x,y);
            }
            
            public void pickRobot(int sx,int sy,int fx,int fy){
                        robotEngine.step(sx,sy);
                        robotEngine.pick(true);
                        robotEngine.step(fx,fy);
                        robotEngine.pick(false);
            }
            
}
