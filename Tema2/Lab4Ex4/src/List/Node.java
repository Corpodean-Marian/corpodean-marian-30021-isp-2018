package List;


public class Node {
                private Node next=null;
                private int key;
                public Node(Node n,int k){
                    next=n;
                    key=k;
                }
                public void setNext(Node n){
                    next=n;
                }
                public Node getNext(){
                    return next;
                }
                public int getKey(){
                    return key;
                }
}
